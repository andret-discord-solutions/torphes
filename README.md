# Torphes

## Developing

1. Check out the repo
2. Install dependencies with `./gradlew build` command.
3. Copy `src/main/resources/config.sample.properties` to `src/main/resources/config.properties`.
4. Ask author about the token to put into the copied file.
5. Start the app.

## Using

[ADD THIS BOT TO YOUR SERVER](https://discord.com/application-directory/606928324970938389)
