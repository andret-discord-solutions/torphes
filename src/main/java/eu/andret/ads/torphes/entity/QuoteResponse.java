package eu.andret.ads.torphes.entity;

import org.jetbrains.annotations.NotNull;

public record QuoteResponse(@NotNull String content) {
}
