package eu.andret.ads.torphes.entity;

public enum Advancement {
	BASIC,
	MEDIUM,
	EXPERT
}
